/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: '#27A376',
        success: '#0CAF60',
        error: '#E03137'
      },
      spacing: {
        '50px': '50px',
        '80px': '80px'
      },
      width: {
        70: '17.5rem', /* 280px */
        25: '6.25rem', /* 100px */
        100: '30rem', /* 480px */
      },
      borderWidth: {
        '5px': '5px'
      },
      borderRadius: {
        '10px': '10px'
      }
    },
  },
  plugins: [],
}
