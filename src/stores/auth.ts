import { defineStore } from 'pinia';

export const useAuthStore = defineStore('auth', {
  state: () => ({
    auth: {}
  }),
  actions: {
    setAuth(auth: object) {
      this.auth = auth;
    },
  },
  getters: {
    getAuth(): object {
      return this.auth
    },
  },
});
