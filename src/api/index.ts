import axios, { AxiosError } from 'axios'
import { useAuthStore } from '../stores/auth'

export const loginRequest = async (payload: {}) => {
  try {
    const { data } = await axios.post('https://fepruebatecnicaculqi-backend-production.up.railway.app/auth/login', payload)
    return data
  } catch (error) {
    const err = error as AxiosError
    return err.response?.data
  }
}

export const requestEmployees = async () => {
  const store = useAuthStore()

  try {
    const { data } = await axios.get('https://fepruebatecnicaculqi-backend-production.up.railway.app/empleados?limit=11&page=4', {
      headers: {
        Authorization: `Bearer ${ store.auth.token }`
      }
    })
    return data
  } catch (error) {
    const err = error as AxiosError
    return err.response?.data
  }
}